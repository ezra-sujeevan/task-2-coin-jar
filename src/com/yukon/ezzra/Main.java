package com.yukon.ezzra;

import com.sun.org.apache.bcel.internal.generic.NEW;

import java.util.Scanner;

public class Main {

    // All measurement is done in mm3

    public static final double jar_volume = 960000.0; //1 Fluid Oz ~ 30 cm3 | 1 cm3 = 1000 mm3

    public static final double cent_volume = 433.23489462836; // diameter = 19.05 mm | thickness = 1.52 mm

    public static final double nickel_volume = 688.97875394097; // diameter = 21.21 mm | thickness = 1.95 mm

    public static final double dime_volume = 340.10641343226; // diameter = 17.91 mm | thickness = 1.35 mm

    public static final double quarter_volume = 808.92735719596; // diameter = 24.26 mm | thickness = 1.75 mm

    public static final double half_volume = 1582.1767579635; // diameter = 30.61 mm | thickness = 2.15 mm

    public static final double dollar_volume = 1103.7578386652; // diameter = 26.49 mm | thickness = 2 mm

    public static final double cent_value = 0.01;

    public static final double nickel_value = 0.05;

    public static final double dime_value = 0.10;

    public static final double quarter_value = 0.25;

    public static final double half_value = 0.5;

    public static final double dollar_value = 1.0;


    public static void main(String[] args) {

        System.out.println("Welcome to Coin Jar Collector");

        double total_value = 0.0;
        double total_volume = 0.0;

        while (true) {

            System.out.println("Total amount available $" + total_value);
            System.out.println("1 - cent ($ 0.01)");
            System.out.println("2 - nickel ($ 0.05)");
            System.out.println("3 - dime  ($ 0.1)");
            System.out.println("4 - quarter ($ 0.25)");
            System.out.println("5 - half ($ 0.5)");
            System.out.println("6 - dollar ($ 1.0)");
            System.out.println("9 - Empty jar");
            System.out.println("0 - Exit application");
            System.out.print("Please Enter a coin to add to the jar : ");


            try {
                Scanner scanner = new Scanner(System.in);
                int selected = scanner.nextInt();
                switch (selected) {
                    case 0:
                        System.out.println("Amount in Jar $" + total_value);
                        System.out.println("Emptying Jar....");
                        System.out.println("Thank You for Using Coin Jar!");
                        System.exit(0);
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        if (jar_volume > total_volume) {
                            switch (selected) {
                                case 1:

                                    if (jar_volume < total_volume + cent_volume) {
                                        System.out.println("Jar is Full cannot add any more cents");
                                    } else {
                                        total_value += cent_value;
                                        total_volume += cent_volume;
                                    }
                                    break;
                                case 2:
                                    if (jar_volume < total_volume + nickel_volume) {
                                        System.out.println("Jar is Full cannot add any more nickels");
                                    } else {
                                        total_value += nickel_value;
                                        total_volume += nickel_volume;
                                    }
                                    break;
                                case 3:
                                    if (jar_volume < total_volume + dime_volume) {
                                        System.out.println("Jar is Full cannot add any more dimes");
                                    } else {
                                        total_value += dime_value;
                                        total_volume += dime_volume;
                                    }
                                    break;
                                case 4:
                                    if (jar_volume < total_volume + quarter_volume) {
                                        System.out.println("Jar is Full cannot add any more quarters");
                                    } else {
                                        total_value += quarter_value;
                                        total_volume += quarter_volume;
                                    }
                                    break;
                                case 5:
                                    if (jar_volume < total_volume + half_volume) {
                                        System.out.println("Jar is Full cannot add any more half dollars");
                                    } else {
                                        total_value += half_value;
                                        total_volume += half_volume;
                                    }
                                    break;
                                case 6:
                                    if (jar_volume < total_volume + dollar_volume) {
                                        System.out.println("Jar is Full cannot add any more dollars");
                                    } else {
                                        total_value += dollar_value;
                                        total_volume += dollar_volume;
                                    }
                                    break;
                            }

                        }
                        else {
                            System.out.println("Jar is Full!!");
                        }
                        break;
                    case 9:
                        System.out.println("Emptying Jar....");
                        total_value = 0.0;
                        total_volume = 0.0;
                        break;
                    default:
                        System.out.println("Incorrect Option Selected!");
                        System.out.println("Please Select Again");
                }


            } catch (Exception e) {
                System.out.println("Please enter a number!");
            }


        }

    }
}
